﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encog;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.ML.Data.Image;
using Encog.Util.DownSample;
using System.Drawing;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.Engine.Network.Activation;
using Encog.ML.Train;
using Encog.Util.Simple;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using System.Windows.Forms;
using System.IO;
namespace Text_Recognition_Test
{
    class Text_Recognition
    {
        static BasicNetwork[] nets;
        static BasicMLDataSet training;
        static string[] words = //даже с таким количеством букв определяет плохо
        {
            "a","b","c","d",
            "e","f","g","h",
            "i","j","k","l"
           // "o","p","q","r",
           // "s","t","u","v",
           // "w","x","y","z",
           // "0","1","2","3",
          //  "4","5","6","7",
          //  "8","9"
        };

        static string[] imageNames =
        {
                ".png",
                "1.png",
                "2.png",
                "3.png",
                "4.png",
                "5.png",
                "6.png",
                "7.png",
                "8.png",
                "9.png"
        };

        #region Functions
        static double[] calcInput(Bitmap b) //преобразование изображения в массив данных
        {
            double[] input = new double[b.Height * b.Width];
            int x = 1, y = 1;
            for (x = 1; x < b.Width; x++)
            {
                for (y = 1; y < b.Height; y++)
                {
                    input[x + y - 2] = b.GetPixel(x, y).GetBrightness();
                }
            }
            return input;
        }

        public static Bitmap DownSampler(Bitmap b) //магия вне хогвартса
        {
            //b = new Bitmap(b, 30, 30);
            int cutheight = b.Height;
            int cutwidth = b.Width;
            int locationx = 0, locationy = 0;
            int miny = b.Height, minx = b.Width;
            bool isXFound = false;
            bool isYFound = false;
            for (int x = 1; x < b.Width; x++) // находим крайний пиксель сверху по оси x
            {
                for (int y = 1; y < b.Height; y++)
                {
                    if (b.GetPixel(x, y).GetBrightness() < 0.9F) // если черный
                    {
                        if (!isXFound)
                        {
                            minx = x; 
                            isXFound = true; //не break, потому-что находится в цикле
                        }
                        
                    }
                }
            }
            for (int y = 1; y < b.Height; y++) // находим крайний пиксель по оси y
            {
                for (int x = 1; x < b.Width; x++)
                {
                    if (b.GetPixel(x, y).GetBrightness() < 0.9F) 
                    {
                        if (!isYFound)
                        {
                            miny = y;
                            isYFound = true;
                        }

                    }
                }
            }
            locationx = minx; // позиция по верхним границам изображения
            locationy = miny;

            cutheight -= miny; // убираем ненужные края
            cutwidth -= minx;
            
            isXFound = false; // обнуляем переменные
            isYFound = false;


            for (int x = b.Width - 1/*-1 потому что кидает эксепшн */; x >= 1; x--) //находим нижнюю границу обрезания
            {
                for (int y = b.Height - 1; y >= 1; y--)
                {
                    if (b.GetPixel(x, y).GetBrightness() < 0.9F)
                    {
                        if (!isXFound)
                        {
                            minx = x;
                            isXFound = true;
                        }

                    }
                }
            }
            for (int y = b.Height -1; y >= 1; y--)
            {
                for (int x = b.Width -1; x >= 1; x--)
                {
                    if (b.GetPixel(x, y).GetBrightness() < 0.9F)
                    {
                        // b.SetPixel(x, y, Color.Black);
                        if (!isYFound)
                        {
                            miny = y;
                            isYFound = true;
                        }

                    }
                }
            }
            cutheight -= b.Height - miny - 1;
            cutwidth -=  b.Width - minx - 1;
            Bitmap bmp = new Bitmap(cutwidth, cutheight); //создаем пустой рисунок
            
            Graphics g = Graphics.FromImage(bmp);
            

            g.DrawImage(b, 0, 0, new Rectangle(locationx, locationy, cutwidth, cutheight), GraphicsUnit.Pixel); //маагия! Изображение обрезано

            bmp = new Bitmap(bmp, 15, 15); //сжимаем до 15 и 15
            for (int x = 1; x < bmp.Width; x++) //т.к. при сжимании появляются серые пиксели, убираем их
            {
                for (int y = 1; y < bmp.Height; y++)
                {
                    if (bmp.GetPixel(x, y).GetBrightness() <= 0.8F)
                    {
                        bmp.SetPixel(x, y, Color.Black);
                    }
                    else
                    {
                        bmp.SetPixel(x, y, Color.White);
                    }
                }
            }
           // bmp.Save("bmp.bmp");
            return bmp;
        }
        #endregion

        public static void TrainNetworks()
        {
            nets = new BasicNetwork[words.Length];
            Bitmap b;
            Random r = new Random();
            
            for (int cnt = 0; cnt < words.Length; cnt++) //зря я делал много сетей.....
            {
                training = new BasicMLDataSet();
                string path = "C:\\Users\\user\\Desktop\\chars\\";
                //заполняем данные для тренировки
                for (int i = 0; i < imageNames.Length; i++)
                {

                    b = Bitmap.FromFile(path + words[cnt] + imageNames[i]) as Bitmap;
                    b = DownSampler(b);
                    BasicMLData data = new BasicMLData(calcInput(b));
                    BasicMLData ideal;
                    ideal = new BasicMLData(new double[] { 1 });
                    training.Add(data, ideal);
                }

                for (int i = 0; i < words.Length; i++) 
                {
                    if (i != cnt)
                    {
                        for (int a = 0; a < 5; a++)
                        {
                            b = Bitmap.FromFile(path + words[i] + imageNames[a]) as Bitmap;
                            b = DownSampler(b);
                            BasicMLData data = new BasicMLData(calcInput(b));
                            BasicMLData ideal;
                            ideal = new BasicMLData(new double[] { 0 });
                            training.Add(data, ideal);
                       }
                           
                    }
                }

                BasicNetwork net = new BasicNetwork();
                net.AddLayer(new BasicLayer(null, false, training.InputSize));
                net.AddLayer(new BasicLayer(new ActivationSigmoid(), true, words.Length + 1));
                net.AddLayer(new BasicLayer(new ActivationSigmoid(), false, 1));
                net.Structure.FinalizeStructure();
                net.Reset();

                IMLTrain train = new ResilientPropagation(net, training);

                int epoch = 1;

                do
                {
                    train.Iteration();
                    Console.WriteLine(@"Epoch #" + epoch + @" Error:" + train.Error);
                    epoch++;
                } while (train.Error > 0.011); //да да, ошибка большая, но по другому не тренируется

                train.FinishTraining();

                // test the neural network
                Console.WriteLine(@"Neural Network Results:");
                foreach (IMLDataPair pair in training)
                {
                    IMLData output = net.Compute(pair.Input);
                    Console.WriteLine(pair.Input[0] + @"," + pair.Input[1]
                                      + @", actual=" + output[0] + @",ideal=" + pair.Ideal[0]);
                }
                nets[cnt] = net;
            }

        }

        /*
         * Сохранить: 
         * FileInfo networkFile = new FileInfo(@"network.eg");
         * Encog.Persist.EncogDirectoryPersistence.SaveObject(networkFile, (BasicNetwork)nets[0]);
         * 
         * Загрузить:
         * network = (BasicNetwork)(Encog.Persist.EncogDirectoryPersistence.LoadObject(networkFile));
         * 
         */

        public static void SaveNetworks()
        {
            for (int i = 0; i < words.Length; i++) //тут думаю все понятно
            {
                FileInfo networkFile = new FileInfo(words[i] + ".eg");
                Encog.Persist.EncogDirectoryPersistence.SaveObject(networkFile, (BasicNetwork)nets[i]);
            }
        }

        public static void LoadNetworks()
        {
            nets = new BasicNetwork[words.Length];
            for (int i = 0; i < words.Length; i++)
            {
                FileInfo networkFile = new FileInfo(words[i] + ".eg");
                nets[i] = (BasicNetwork)(Encog.Persist.EncogDirectoryPersistence.LoadObject(networkFile));
            }

        }

        public static char UseNetworks(Bitmap b)
        {
            double d;
            for (int i = 0; i< words.Length; i++)
            {
                d = nets[i].Compute(new BasicMLData(calcInput(DownSampler(b))))[0]; //получаем значение 0 нейрона на выходе
                if (d >= 0.5 && d <= 1)
                {
                    return words[i][0];
                }
            }
            return '/'; // если не одна из сетей не вернула число больше 0.5, но меньше 1
        }
    }
}
