﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Text_Recognition_Test
{
    public partial class Form1 : Form
    {
        Graphics pbGraphics, bGraphics;
        Bitmap b;
        public Form1()
        {
            InitializeComponent();
            b = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            pictureBox2.Image = b;
            bGraphics = Graphics.FromImage(b);
            pbGraphics = pictureBox2.CreateGraphics();
            pbGraphics.Clear(Color.White);
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            Text_Recognition.TrainNetworks();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            b = Bitmap.FromFile(openFileDialog1.FileName) as Bitmap;
            pictureBox1.Image = Text_Recognition.DownSampler(b);
        }

        private void button3_Click(object sender, EventArgs e)
        {
           // g = pictureBox2.CreateGraphics();
            
            pictureBox2.Image = b;
            pictureBox1.Image = Text_Recognition.DownSampler(b);
            MessageBox.Show(Text_Recognition.UseNetworks(b).ToString());
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Text_Recognition.LoadNetworks();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Text_Recognition.SaveNetworks();
        }
        bool flag = false;
        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            Console.WriteLine("asds");
            flag = !flag;
            //g.FillRectangle(Brushes.Black, 100, , 100, 100);
        }

        private void PictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            Console.WriteLine("asds");
            flag = !flag;
            //g.FillRectangle(Brushes.Black, 100, , 100, 100);
        }

        private void PictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
          // pbGraphics = pictureBox2.CreateGraphics();
            if (flag)
            {
                pbGraphics.FillEllipse(Brushes.Black, new Rectangle(e.X, e.Y, 4, 4));
                bGraphics.FillEllipse(Brushes.Black, new Rectangle(e.X, e.Y, 4, 4));
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

            b = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            pictureBox2.Image = b;
            bGraphics = Graphics.FromImage(b);
            pbGraphics.Clear(Color.White);
            bGraphics.Clear(Color.White);
        }
    }
}
